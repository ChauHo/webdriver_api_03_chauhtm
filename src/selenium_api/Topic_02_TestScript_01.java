package selenium_api;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Topic_02_TestScript_01 {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		// Open browser
		driver = new FirefoxDriver();

	}

	@BeforeMethod
	public void OpenApp() {
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	// @Test
	public void TC_01() {
		// Check title Homepage
		String titlename = driver.getTitle();
		Assert.assertEquals("Home page", titlename);

		// Move to page login
		driver.findElement(By.xpath("//a[@class='skip-link skip-account']")).click();
		driver.findElement(By.xpath("//a[@title='Log In']")).click();

		// Move to page create account
		driver.findElement(By.xpath("//a[@title='Create an Account']")).click();

		// Back to page login
		driver.navigate().back();
		String strUrlLogin = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/login/", strUrlLogin);

		// Forward to page create account
		driver.navigate().forward();
		String strUrlCreate = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", strUrlCreate);
		// driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// @Test
	public void TC_02() {
		driver.findElement(By.xpath("//a[@class='skip-link skip-account']")).click();
		driver.findElement(By.xpath("//a[@title='Log In']")).click();
		driver.findElement(By.xpath("//button[@title='Login']")).click();

		// Verify error content at Email Address
		String strErrorEmail = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("This is a required field.", strErrorEmail);

		// Verify error content at Password
		String strErrorPassword = driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", strErrorPassword);
	}

//	@Test
	public void TC_03() {
		String email = "123434234@12312.123123";
		String errorMessage = "Please enter a valid email address. For example johndoe@domain.com.";

		driver.findElement(By.xpath("//a[@class='skip-link skip-account']")).click();
		driver.findElement(By.xpath("//a[@title='Log In']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);
		driver.findElement(By.xpath("//button[@title='Login']")).click();
		String strErrorEmail = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.", strErrorEmail);
		Assert.assertEquals(errorMessage, strErrorEmail);

	}
	
	@Test
	public void TC_04() {
		String email = "automation@gmail.com";
		String pass = " 123";
		//Wrong text
		//String message = "Thank you for registering with Main Website Store.";
		String message = "Please enter 6 or more characters without leading or trailing spaces.";
		driver.findElement(By.xpath("//a[@class='skip-link skip-account']")).click();
		driver.findElement(By.xpath("//a[@title='Log In']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);		
		//driver.findElement(By.xpath("//a[@title='Log In']")).click(); Delete this 
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(pass);			
		driver.findElement(By.xpath("//button[@title='Login']")).click();
		// Wrong xpath 
		String strErrorEmail = driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']")).getText(); 
		Assert.assertEquals(strErrorEmail,message);
	}
	
	@Test
	public void TC_05() {
		String email = "automation@gmail.com";
		String pass = "123";
		String message = "Thank you for registering with Main Website Store.";

		driver.findElement(By.xpath("//a[@class='skip-link skip-account']")).click();
		driver.findElement(By.xpath("//a[@title='Log In']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);		
		//driver.findElement(By.xpath("//a[@title='Log In']")).click(); Delete this
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(pass);			
		driver.findElement(By.xpath("//button[@title='Login']")).click();
		String strErrorEmail = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("Please enter 6 or more characters. Leading or trailing spaces will be ignored.", strErrorEmail);
		Assert.assertEquals(message, strErrorEmail);
	}

	@AfterClass
	public void afterClass() {
		// Close browser
		//driver.quit();
	}
}
