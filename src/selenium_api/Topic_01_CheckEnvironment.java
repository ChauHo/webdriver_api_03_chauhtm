package selenium_api;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_01_CheckEnvironment {
	
	WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
		//Open browser
		driver = new FirefoxDriver();
		
		//Open app
		driver.get("http://live.guru99.com/");
		
		//Wait page load successfull
		driver.manage().timeouts().implicitlyWait(15,  TimeUnit.SECONDS);
		
		//Maximize browser fullscreen
		driver.manage().window().maximize();
	}

	@Test
	public void TC_01() {
		//Get current title
		String homePageTitle = driver.getTitle();
		
		//Verify actual title with expected title
		Assert.assertEquals("Home page", homePageTitle);
		
		//Email adress textbox
		/*
		<input id="email" 
		class="input-text required-entry validate-email"
		type="email" 
		title="Email Address" 
		value="" name="login[username]" 
		spellcheck="false" 
		autocorrect="off" 
		autocapitalize="off"/>
		*/
		//tagname[@attribute='value']
		
		driver.findElement(By.id("email"));
		driver.findElement(By.className("input-text required-entry validate-email"));
		driver.findElement(By.name("login[username]"));
		driver.findElement(By.cssSelector("input[name='login[username]']"));
		driver.findElement(By.tagName("a"));
		driver.findElement(By.linkText("About Us"));
		driver.findElement(By.partialLinkText("About"));
		
		driver.findElement(By.xpath("//*[@id='email']"));
		driver.findElement(By.xpath("//*[@class='input-text required-entry validate-email']"));
		driver.findElement(By.xpath("//*[@name='login[username]']"));
		
		String customerID = driver.findElement(By.xpath("//td[contains(text(),'Customer ID')]/following-sibling::td")).getText();
		System.out.println(customerID); // => 4742
		
		String total = driver.findElement(By.xpath("//span[contains(text(),'Total')].preceeding-sibling::span")).getText();
		System.out.println(total);
	}

	@AfterClass
	public void afterClass() {
		//close browser
		driver.quit();
	}

}
